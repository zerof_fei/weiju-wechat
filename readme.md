## 微距

当你在城市打拼，独自在一个人 🧑 总有某些时刻你会想：为什么总是一个人？为什么自己要到这里？
手机中的社交软件一大堆，却总是找不到一个知己，以前总是认为平时一个人挺好的，可是现在一个人生病的话，就显得非常难受了。
多么想有那么一个人来照顾自己。不论性别与否，男性朋友、女性朋友、男朋友或女朋友等，多么希望有一个人来看望自己啊。☹😳

网络社交越来越发达，人与人之间的隔阂是越来越重，当然自己的圈子是越来越小了。我想解决这一痛点，便诞生出了一个想法，就比如一个人想看个电影，感觉有点奇怪。于是我想打造一个真实为前提，互帮互助的一个平台。但并不想打造一个社交平台，或者一个群体。群体一旦人一多，根本无法控制其内容。以前加过许多学习群，特别是人多的群，大多是装逼成风，毫无学习气氛。这个情况的群多半是死亡:skull:状态了。

我的想法是这样的：在平台上发表自己的诉求，一旦有人有诉求或意向寻求帮助，便可进入类似聊天室的虚拟房间进行聊天，其他人就无法进入查看其聊天内容。 
陪伴是个互相的过程，两个孤独的陌生人碰在一起（不是相亲啊，但也不反对:flushed:  ）,我孤独的时候总想出去走走玩玩，但是想想一个人有啥意思呀。我自己深有体会呀。希望·······（大家自己想想）



> 时间 2021年3月29日15:32:45      我们要用uni-app 开始重构了，等待吧！！🙌



## 汇溪和他们的小伙伴们

这个小程序到的粗糙版本出来了，云开发版本的给撸出来了。反正就不是很满意，它不完美，不完善，UI很丑，一切都是将就下来的结果。之后打算用**uni-app**重构了。只要项目足够的完善，一定能搞到**1000start**💕


> Java后台地址： https://gitee.com/huixi_and_their_friends/weiju.git







## 关于整个的结构图（使用幕布完成）

> 结构图地址：https://mubu.com/doc/aZ6rrEPIfw

![微距结构图](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/%E5%BE%AE%E8%B7%9D2020.2.7.png)



## 小程序原型图



> https://lanhuapp.com/url/WfEvI   
> 密码: CAkU
>
> 蓝湖有人员限制，只能分享这个链接。不能通过这个链接的页面访问标注信息，很可惜。



> https://weiju1.oss-cn-shenzhen.aliyuncs.com/Zingo%20Social%20UI%20Kit_Dark%20theme.pdf   （PDF）



> https://weiju1.oss-cn-shenzhen.aliyuncs.com/Zingo%20Social%20UI%20Kit_Dark%20theme.sketch  (sketch)   Mac 用户可以下载这个 **sketch** 打开；Windows 可以用 **Lunacy** 打开

## 部分原型页面展示

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-16.png)

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-34.png)

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-45.png)

![img](https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/Snipaste_2019-10-23_11-42-51.png)